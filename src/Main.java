import java.util.Scanner;

public class Main {

    public static int fact(int a) {
        if (a==1) return 1;
        return a*fact(a-1);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();

        System.out.println(fact(a));
    }
}
